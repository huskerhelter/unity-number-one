﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour {

    public int damage;
    public int point;

    public GameObject projectile;
    public int projectilePoolSize;
    private List<GameObject> projectilePool;

	// Use this for initialization
	private void Start () {
        projectilePool = new List<GameObject>();

        for (int x = 0; x< projectilePoolSize; x++)
        {
            GameObject go = Instantiate(projectile);
            go.SetActive(false);

            projectilePool.Add(go);
        }
	}
	
	// Update is called once per frame
	void Update () {


        if (Input.GetButtonDown("Fire1"))
        { 
            if(projectile != null)
            {
                Vector3 playerPos = transform.position;
                Vector3 playerDirection = transform.forward;
                Quaternion playerRotation = transform.rotation;

                Vector3 spawnPos = playerPos + playerDirection * 2;

                foreach(GameObject go in projectilePool)
                {
                    if (!go.activeSelf)
                    {
                        go.transform.position = spawnPos;
                        go.transform.rotation = playerRotation;
                        go.SetActive(true);
                        go.GetComponent<Projectile>().Fire();
                        break;
                    }
                }

                //GameObject bullet = Instantiate(projectile, spawnPos, playerRotation);
                //bullet.GetComponent<Projectile>().Fire();
            }
        }

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            EnemyController enemy = collision.gameObject.GetComponent<EnemyController>();
            enemy.GetHit(damage, point);
        }
        Debug.Log(collision.gameObject.name);
    }
}
