﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController2 : MonoBehaviour {

    public int damage;
    public int point;

	void Update () {
        float x = Input.GetAxis("Horizontal") * Time.deltaTime * 150f;
        float z = Input.GetAxis("Vertical") * Time.deltaTime * 3.0f;

        transform.Rotate(0, x, 0);
        transform.Translate(0, 0, z);
	}

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            EnemyController enemy = collision.gameObject.GetComponent<EnemyController>();
            enemy.GetHit(damage, point);
        }
        Debug.Log(collision.gameObject.name);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "point")
        {

            Debug.Log(other.gameObject.name);
        }
    }
}
