﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {

    public int health;

    public int point1 = 10;

    public void GetHit(int damage, int point)
    {
        point += point1;
        health -= damage;

        if(health < 1)
        {
            Debug.Log(point);
            Destroy(this.gameObject);
        }
    }
}
