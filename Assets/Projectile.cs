﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

    private Rigidbody rb;
    public float speed;
    public float lifeTime;

    private void Awake()
    {
        rb = gameObject.GetComponent<Rigidbody>();

    }

    public void Fire()
    {
        rb.AddForce(transform.forward * speed);
        StartCoroutine(RecycleObject(lifeTime));
    }

    private IEnumerator RecycleObject(float expire)
    {
        yield return new WaitForSeconds(expire);
        rb.velocity = Vector3.zero;
        gameObject.SetActive(false);
    }

}
